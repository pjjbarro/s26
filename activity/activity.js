let http = require('http');

let server = http.createServer(function(req,res) {
	if(req.url === '/'){

		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('Welcome to Our Page!');
	}

	else if (req.url === '/login'){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('Welcome to the Login Page!');
	}

	else if (req.url === '/register'){
		res.writeHead(404, {'Content-Type': 'text/plain'});
		res.end('Welcome to the Register Page. Please register your details.');
	}

	else {
	res.writeHead(404,{'Content-Type': 'text/plain'});
	res.end('Resource Cannot Be Found')
	}


}).listen(4000);
console.log('Server running at localhost:40000');