/*
	What is Node.js?

	Node.js was created because Javascript was originally created and conceptualized to be used for Front-End Development.
	Nodejs is an open source environment which allows to create backend applications using JS or Javascript.

	Note: Front-end is usually the page that we're seeing. it is usally a client that requests resources from a Back-End.
	Back-end is usually a server or api that serves and responds to the Front-End.

	Why is Node.js popular?

	Performance - nodejs is one of the most performing environment for creating backend applications for JS.

	NPM - Node Package Manager - it is one of the largest registry for packages. Packages are methods,functions,codes that greatly helpsor adds with applications.

	Familiarity - JS is one of the most popular programming langauges and Node.js uses JS as its primary language.

*/

/*

	We used require () method to load node.js modules 
		A module is a software component or part of a program which contains one or more routines.
		
		8000, 5000 is also available.

		.createServer() has an anonymous function that handles our client request and server response. The anonymous function actually recieves a request object which contains details about a client's request and a response object which contains details and methods for server response.


*/
let http - require("http");

http.createServer(function(request,response){
	if(req.url === "/") {
	//.writeHead() is a method of the response object. This will allows us to add headers which are additional information about the servers response. The first argument of writeHead() is HTTP status for our response. 200 - OK
	//The second argument as header. Content-Type allows us to tell the client that the income response they will receive is text format.
	response.writeHead(200,{'Content-Type': 'text/plain'});
	//.end() ends the response of the server and sends the data or message to the client.
	response.end('Hello World!');

}

else if(req.url === "/Jaybee"){
	res.writeHead(200,{'Content-Type': 'text/plain'});
	res.end('Hi, I am Jaybee!')
} 


else {

	//This will be our response endpoint passed in the client's request is not recognized or there is no designated route for this endpoint


	res.writeHead(404,{'Content-Type': 'text/plain'});
	res.end('Resource Cannot Be Found')
}




}).listen(4000);

console.log('Server running at localHost:4000');


